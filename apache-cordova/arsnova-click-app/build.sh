#!/usr/bin/env bash

set -e

CORDOVA_DIR="$PWD"

if [[ -z "$SERVER_DOMAIN" ]]; then
   echo "SERVER_DOMAIN not set!"
   exit 1
fi

# Run angular build
pushd ../..
   npm install || exit 1
   sed -i "s|import 'mousetrap';|import { MousetrapInstance } from 'mousetrap';|g" "node_modules/angular2-hotkeys/lib/hotkeys.service.d.ts"

   echo "Building unique version hash for the build"
   HASH=$(date | md5sum | head -c32)
   echo $HASH > src/assets/version.txt

   sed -i src/environments/environment*.ts \
      -e"s|^  serverEndpoint: '.*',\$|  serverEndpoint: 'https://$SERVER_DOMAIN/backend',|" \
      -e"s|^  version: '.*',\$|  version: '$HASH',|" \
      -e"s|^    endpoint: '.*',\$|    endpoint: 'wss://$SERVER_DOMAIN/rabbitmq/ws',|"
   npm run -- ng build --prod --base-href . --output-path apache-cordova/arsnova-click-app/www || exit 2

   mkdir -p ./dist/frontend
   ln -s "$CORDOVA_DIR/www" ./dist/frontend/browser
   npm run -- ng run frontend:server:production
   baseDir="$PWD" buildDir="$PWD/apache-cordova/arsnova-click-app/www" ./build-assets.sh "${NPM_BUILD_TYPE:-PROD}" "https://$SERVER_DOMAIN"
popd

# Change absolute paths to relative ones
sed -i www/index.html -e"s|/theme-default.css|./theme-default.css|g"
find www -type f -exec sed -e's|(/assets/|(./assets/|g' -i {} \;
sed -e's|:"/|:"./|g' -i www/assets/meta/*/linkNodes.json

# Check icon image file
icon_file_default=$(grep -Po '(?<=<icon src=")[^"]*(?=" />)' ./config.xml)
if [[ ! -f "$icon_file_default" ]]; then
   icon_theme_dir="${icon_file_default%/*/*}"
   icon_theme_fallback="westermann-blue"

   if [[ -d "$icon_theme_dir/$icon_theme_fallback" ]]; then
      icon_file="$icon_theme_dir/$icon_theme_fallback/${icon_file_default##*/}"
      echo "Using icon from theme $icon_theme_fallback (default not found: '$icon_file_default')"
   else
      icon_file="$icon_theme_dir/$(ls -1 "$icon_theme_dir" | head -n1)/${icon_file_default##*/}"
      echo "Warning: Default app icon file not found: '$icon_file_default'; using '$icon_file' instead."
   fi

   sed -i "s|\(<icon src=\"\)[^\"]*\(\" />\)|\1$icon_file\2|" ./config.xml
fi
unset -v icon_file icon_file_default icon_theme_dir icon_theme_fallback

# Run cordova build
npm install -g cordova
cordova prepare
cordova compile ${CORDOVA_BUILD_TYPE:+"--$CORDOVA_BUILD_TYPE"}
