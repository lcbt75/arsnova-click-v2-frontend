import { IEnvironment } from '../app/lib/interfaces/IEnvironment';

export const environment: IEnvironment = {
  production: false,
  serverEndpoint: 'http://localhost:3010',
  stompConfig: {
    endpoint: 'ws://localhost:15674/ws',
    user: 'arsnova-click',
    password: 'K3BHZQMHsxh6XQ5a',
    vhost: '/',
  },
  version: '__VERSION__',
};
