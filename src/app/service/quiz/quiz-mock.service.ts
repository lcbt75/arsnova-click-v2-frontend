import { Observable, of, ReplaySubject } from 'rxjs';
import { FreeTextAnswerEntity } from '../../lib/entities/answer/FreetextAnwerEntity';
import { AbstractQuestionEntity } from '../../lib/entities/question/AbstractQuestionEntity';
import { FreeTextQuestionEntity } from '../../lib/entities/question/FreeTextQuestionEntity';
import { RangedQuestionEntity } from '../../lib/entities/question/RangedQuestionEntity';
import { SingleChoiceQuestionEntity } from '../../lib/entities/question/SingleChoiceQuestionEntity';
import { SurveyQuestionEntity } from '../../lib/entities/question/SurveyQuestionEntity';
import { QuizEntity } from '../../lib/entities/QuizEntity';
import { SessionConfigurationEntity } from '../../lib/entities/session-configuration/SessionConfigurationEntity';
import { IFooterBarElement } from '../../lib/footerbar-element/interfaces';
import {SettingsService} from '../settings/settings.service';

export class QuizMockService {
  public quiz: QuizEntity;

  public quizUpdateEmitter = new ReplaySubject(1);

  public isOwner = true;

  constructor(private settingsService: SettingsService) {
    this.quiz = new QuizEntity(this.settingsService, {
      name: 'test',
      sessionConfig: new SessionConfigurationEntity(
        this.settingsService,
        JSON.parse(JSON.stringify(this.settingsService.frontEnv?.defaultQuizSettings.sessionConfig))
      ),
      currentQuestionIndex: 0,
      questionList: [
        new SingleChoiceQuestionEntity(this.settingsService, {}),
        new FreeTextQuestionEntity(this.settingsService, {
          questionText: '',
          timer: 0,
          answerOptionList: [
            new FreeTextAnswerEntity(this.settingsService, {
              answerText: '',
              configCaseSensitive: true,
              configTrimWhitespaces: true,
              configUseKeywords: true,
              configUsePunctuation: true,
            }),
          ],
        }), new RangedQuestionEntity(this.settingsService, {
          questionText: '',
          timer: 0,
          correctValue: 20,
          rangeMin: 10,
          rangeMax: 30,
        }), new SurveyQuestionEntity(this.settingsService, {
          questionText: '',
          timer: 0,
          displayAnswerText: true,
          answerOptionList: [],
          showOneAnswerPerRow: true,
          multipleSelectionEnabled: false,
        }),
      ],
    });
    this.quizUpdateEmitter.next(this.quiz);
  }

  public currentQuestion(): AbstractQuestionEntity {
    return this.quiz.questionList[0];
  }

  public getVisibleQuestions(): Array<AbstractQuestionEntity> {
    return [this.currentQuestion()];
  }

  public cleanUp(): Observable<any> {
    return new Observable(subscriber => subscriber.next());
  }

  public persist(): void {}

  public close(): Observable<any> {
    return new Observable(subscriber => subscriber.next());
  }

  public loadData(): Observable<QuizEntity> {
    return of(this.quiz);
  }

  public loadDataToEdit(val: string): Promise<void> {
    return new Promise<void>(resolve => resolve());
  }

  public generatePrivateKey(): string {
    return 'privateKey';
  }

  public toggleSetting(val: IFooterBarElement): void {}

  public removeSelectedNickByName(val: string): void {}

  public addSelectedNick(val: string): void {}

  public toggleSelectedNick(val: string): void {}

  public isValid(): boolean {
    return true;
  }

  public stopEditMode(): void {}

  public loadDataToPlay(val: IFooterBarElement): Promise<void> {
    return new Promise<void>(resolve => resolve());
  }
}
