import { TestBed } from '@angular/core/testing';

import { MotdDataService } from './motd-data.service';

describe('MotdDataService', () => {
  let service: MotdDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotdDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
