import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { of } from 'rxjs/index';
import { themes } from '../../lib/available-themes';
import {ITwitter} from '../../lib/settings/ITwitter';
import {IGit} from '../../lib/settings/IGit';
import {IWebNotifications} from '../../lib/settings/IWebNotifications';
import {IMiscellaneous} from '../../lib/settings/IMiscellaneous';
import {IEnvironment} from '../../lib/settings/IEnvironment';

@Injectable({
  providedIn: 'root',
})
export class SettingsMockService {
  public twitterSettings: ITwitter;
  public frontEnv: IEnvironment = {
    appName: '',
    availableQuizThemes: [],
    backendVersion: '',
    cacheQuizAssets: false,
    confidenceSliderEnabled: false,
    createQuizPasswordRequired: false,
    darkModeCheckEnabled: false,
    defaultQuizSettings: {
      answers: {
        answerText: '',
        isCorrect: false, // Must be false so that surveys are valid (check if isCorrect is false)
        configCaseSensitive: false,
        configTrimWhitespaces: false,
        configUseKeywords: false,
        configUsePunctuation: false,
      },
      question: {
        dispayAnswerText: true,
        showOneAnswerPerRow: true,
        questionText: '',
        timer: 30,
        multipleSelectionEnabled: true,
        rangeMin: 0,
        rangeMax: 60,
        correctValue: 30,
        answerOptionList: [],
        tags: [],
        requiredForToken: true,
        difficulty: 5
      },
      sessionConfig: {
        music: {
          enabled: {
            lobby: true,
            countdownRunning: true,
            countdownEnd: true,
          },
          shared: {
            lobby: false,
            countdownRunning: false,
            countdownEnd: false,
          },
          volumeConfig: {
            global: 60,
            lobby: 60,
            countdownRunning: 60,
            countdownEnd: 60,
            useGlobalVolume: true,
          },
          titleConfig: {
            lobby: 'Song3',
            countdownRunning: 'Song1',
            countdownEnd: 'Song1',
          },
        },
        nicks: {
          memberGroups: [],
          maxMembersPerGroup: 10,
          autoJoinToGroup: false,
          blockIllegalNicks: true,
          selectedNicks: [],
        },
        theme: themes[0].id,
        readingConfirmationEnabled: true,
        showResponseProgress: true,
        confidenceSliderEnabled: true,
        cacheQuizAssets: false,
      },
    },
    defaultTheme: undefined,
    enableBonusToken: false,
    enableQuizPool: false,
    forceQuizTheme: false,
    infoAboutTabEnabled: false,
    infoBackendApiEnabled: false,
    infoProjectTabEnabled: false,
    leaderboardAlgorithm: '',
    leaderboardAmount: 0,
    limitActiveQuizzes: 0,
    loginButtonLabelConfiguration: '',
    loginMechanism: [],
    markdownFilePostfix: '',
    persistQuizzes: false,
    production: false,
    readingConfirmationEnabled: false,
    requireLoginToCreateQuiz: false,
    showInfoButtonsInFooter: false,
    showJoinableQuizzes: false,
    showLoginButton: false,
    showPublicQuizzes: false,
    stompConfig: { endpoint: '', password: '', user: '', vhost: '' },
    title: '',
    twitterEnabled: false,
    vapidPublicKey: '',
    version: '',
  };
  public gitSettings: IGit;
  public misc: IMiscellaneous;
  public webNotificationSettings: IWebNotifications;
  public settingsLoadedEmitter = of(null);

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
  }

  private async initServerSettings(): Promise<void> {
    return new Promise<void>(resolve => resolve());
  }
}
