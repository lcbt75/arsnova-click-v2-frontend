import { Injectable } from '@angular/core';
import {DefaultSettings} from '../../../lib/default.settings';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../../user/user.service';
import {IGit} from '../../../lib/settings/IGit';
import {IMiscellaneous} from '../../../lib/settings/IMiscellaneous';
import {IWebNotifications} from '../../../lib/settings/IWebNotifications';
import {ITwitter} from '../../../lib/settings/ITwitter';
import { UserRole } from 'src/app/lib/enums/UserRole';
import {Observable, zip} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AdminSettingsApiService {

  public twitterSettings: ITwitter;
  public gitSettings: IGit;
  public misc: IMiscellaneous;
  public webNotificationSettings: IWebNotifications;

  constructor(private http: HttpClient, private userService: UserService) {
    this.init();
  }

  public init(): void {
    zip(
        ['twitter', 'git', 'misc', 'web_notification'].map(v => this.getSettings(v))
    ).subscribe(v => {
      this.twitterSettings = v[0].config;
      this.gitSettings = v[1].config;
      this.misc = v[2].config;
      this.webNotificationSettings = v[3].config;
    });
  }

  public getSettings(name: string): Observable<{configName: string, config: any}> {
    const checkRole = this.userService.isAuthorizedFor([UserRole.ConfAdmin, UserRole.SuperAdmin]);

    if (!checkRole) {
      return undefined;
    }

    return this.http.get<{configName: string, config: any}>(`${DefaultSettings.httpApiEndpoint}/admin/settings/${name}`, {
        headers: { authorization: this.userService.staticLoginToken },
      });
  }
}
