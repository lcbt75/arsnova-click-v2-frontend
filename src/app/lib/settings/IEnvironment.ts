import { LoginMechanism } from '../enums/enums';
import {QuizTheme} from '../enums/QuizTheme';
import {IMemberGroupBase} from '../interfaces/users/IMemberGroupBase';

interface IDefaultAnswerSetting {
    answerText: string;
    isCorrect: boolean;
    configCaseSensitive: boolean;
    configTrimWhitespaces: boolean;
    configUseKeywords: boolean;
    configUsePunctuation: boolean;
}

interface IDefaultQuestionSetting {
    dispayAnswerText: boolean;
    showOneAnswerPerRow: boolean;
    questionText: string;
    timer: number;
    multipleSelectionEnabled: boolean;
    rangeMin: number;
    rangeMax: number;
    correctValue: number;
    answerOptionList: Array<IDefaultAnswerSetting>;
    tags: Array<string>;
    requiredForToken: boolean;
    difficulty: number;
}

interface IDefaultMusicSessionSetting {
    enabled: {
        lobby: boolean;
        countdownRunning: boolean;
        countdownEnd: boolean;
    };
    shared: {
        lobby: boolean;
        countdownRunning: boolean;
        countdownEnd: boolean;
    };
    volumeConfig: {
        global: number;
        lobby: number;
        countdownRunning: number;
        countdownEnd: number;
        useGlobalVolume: boolean;
    };
    titleConfig: {
        lobby: string;
        countdownRunning: string;
        countdownEnd: string;
    };
}

interface IDefaultSessionSetting {
    music: IDefaultMusicSessionSetting;
    nicks: {
        memberGroups: Array<IMemberGroupBase>;
        maxMembersPerGroup: number;
        autoJoinToGroup: boolean;
        blockIllegalNicks: boolean;
        selectedNicks: Array<string>;
    };
    theme: string;
    readingConfirmationEnabled: boolean;
    showResponseProgress: boolean;
    confidenceSliderEnabled: boolean;
    cacheQuizAssets: boolean;
}

interface IDefaultQuizSettings {
    answers: IDefaultAnswerSetting;
    question: IDefaultQuestionSetting;
    sessionConfig: IDefaultSessionSetting;
}

export interface IEnvironment {
    cacheQuizAssets: boolean;
    createQuizPasswordRequired: boolean;
    limitActiveQuizzes: number;
    defaultQuizSettings: IDefaultQuizSettings;
    backendVersion: string;
    enableBonusToken: boolean;
    vapidPublicKey: string;
    title: string;
    appName: string;
    sentryDSN?: string;
    version: string;
    production: boolean;
    stompConfig: {
      endpoint: string, user: string, password: string, vhost: string,
    };
    leaderboardAlgorithm: string;
    leaderboardAmount: number;
    readingConfirmationEnabled: boolean;
    confidenceSliderEnabled: boolean;
    infoAboutTabEnabled: boolean;
    infoProjectTabEnabled: boolean;
    infoBackendApiEnabled: boolean;
    requireLoginToCreateQuiz: boolean;
    showJoinableQuizzes: boolean;
    showPublicQuizzes: boolean;
    forceQuizTheme: boolean;
    loginMechanism: Array<LoginMechanism>;
    showLoginButton: boolean;
    persistQuizzes: boolean;
    availableQuizThemes: Array<QuizTheme>;
    defaultTheme: QuizTheme;
    darkModeCheckEnabled: boolean;
    twitterEnabled: boolean;
    enableQuizPool: boolean;
    showInfoButtonsInFooter: boolean;
    markdownFilePostfix: string;
    loginButtonLabelConfiguration: string;
}
