export class MusicVolumeSessionConfigurationEntity {
  public global: number;
  public lobby: number;
  public countdownRunning: number;
  public countdownEnd: number;
  public useGlobalVolume: boolean;

  constructor(settingsService, props) {
    this.global = props.global ?? settingsService.frontEnv?.defaultQuizSettings.sessionConfig.music.volumeConfig.global;
    this.lobby = props.lobby ?? settingsService.frontEnv?.defaultQuizSettings.sessionConfig.music.volumeConfig.lobby;
    this.countdownRunning = props.countdownRunning ?? settingsService.frontEnv?.defaultQuizSettings.sessionConfig.music.volumeConfig.countdownRunning;
    this.countdownEnd = props.countdownEnd ?? settingsService.frontEnv?.defaultQuizSettings.sessionConfig.music.volumeConfig.countdownEnd;
    this.useGlobalVolume = props.useGlobalVolume ?? settingsService.frontEnv?.defaultQuizSettings.sessionConfig.music.volumeConfig.useGlobalVolume;
  }
}
