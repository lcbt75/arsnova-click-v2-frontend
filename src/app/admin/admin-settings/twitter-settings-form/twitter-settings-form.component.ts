import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AdminApiService} from '../../../service/api/admin/admin-api.service';
import {AdminSettingsApiService} from '../../../service/api/admin/admin-settings-api.service';

@Component({
    selector: 'app-twitter-settings-form',
    templateUrl: './twitter-settings-form.component.html',
    styleUrls: ['./twitter-settings-form.component.scss']
})
export class TwitterSettingsFormComponent implements OnInit {
    public settingsForm: FormGroup;
    public settings: any;
    public isSending = false;

    constructor(
        private fb: FormBuilder,
        private adminApiService: AdminApiService,
        private adminSettingsApi: AdminSettingsApiService
    ) {
    }

    public ngOnInit(): void {
        this.adminSettingsApi.getSettings('twitter').subscribe(settings => {
            this.settings = settings;
            this.settingsForm = this.fb.group({
                twitterEnabled: [settings.config.twitterEnabled, []],
                twitterConsumerKey: [settings.config.twitterConsumerKey, []],
                twitterConsumerSecret: [settings.config.twitterConsumerSecret, []],
                twitterBearerToken: [settings.config.twitterBearerToken, []],
                twitterAccessTokenKey: [settings.config.twitterAccessTokenKey, []],
                twitterAccessTokenSecret: [settings.config.twitterAccessTokenSecret, []],
                twitterSearchKey: [settings.config.twitterSearchKey, []],
            });
        });
    }

    public async submitForm(): Promise<void> {
        this.isSending = true;
        for (const control of Object.values(this.settingsForm.controls)) {
            control.markAsDirty();
            control.updateValueAndValidity();
            if (control.invalid) {
                console.log(control);
            }
        }

        if (this.settingsForm.valid) {
            this.settings.config = {...this.settings.config, ...this.settingsForm.getRawValue()};
            this.adminApiService.editSettings(this.settings).subscribe(res => {
                if (res) {
                    console.log(res);
                    this.adminSettingsApi.twitterSettings = res.config;
                } else {
                    console.log('error udpating settings');
                }
                this.isSending = false;
            });
        }
    }
}

