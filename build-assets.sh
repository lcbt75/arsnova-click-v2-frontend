#!/usr/bin/env bash
set -e

if [[ -z "$baseDir" ]]; then
  baseDir="/usr/src/app"
fi
if [[ -z "$buildDir" ]]; then
   buildDir="$baseDir/dist/frontend/browser"
fi


echo "Copying assets"
npm run copy:"$1"

cd "$baseDir"
echo "Purifying css"
npm run purify

echo "Regenerating ngsw hashes"
npm run ngsw-config

echo "Renaming css files to a hashed version and add a json file containing the theme and the corresponding hash"
cd "$buildDir"
echo "Building css file hashes"
sha1sum theme-*.css > theme-hashes.txt

echo "Renaming the css files with their hash"
while read p; do
  hash=$(echo "$p" | cut -d ' ' -f1)
  file=$(echo "$p" | cut -d ' ' -f3)
  if [[ "$file" != "${file/__CSS_FILE_HASH__/$hash}" ]]; then
     mv "$file" "${file/__CSS_FILE_HASH__/$hash}"
  fi
done < theme-hashes.txt

echo "Building json file which contains a map with the theme name and the current hash"
jq -R -c 'split("\n") | .[] | split(" ") | {hash: .[0], theme: .[2] | rtrimstr("\n") | sub("\\-__CSS_FILE_HASH__.css";"") | sub("theme-";"")}' < theme-hashes.txt | jq -c -s '.' > assets/theme-hashes.json

echo "Checking if the theme assets need to be regenerated"
stylefile=$(ls | grep "styles.*.css" | head -n 1) > /dev/null
csstype="text/css"

if
   curl -sI "$2/$stylefile" | awk -F ': ' '$1 == "content-type" { print $2 }' | grep $csstype > /dev/null &&
   curl -s "$2/assets/theme-hashes.json" | diff - assets/theme-hashes.json > /dev/null
then
   echo "Styles are equal - no need to regenerate the theme assets but we need to download them"

   for theme in $(echo $themes | jq '.[]')
   do
      theme=$(echo "$theme" | tr -d '"')
      echo "Downloading assets for theme '$theme'"
      mkdir -p assets/images/theme/$theme
      mkdir -p assets/meta/$theme

      curl -s $2/assets/meta/$theme/linkNodes.json > assets/meta/$theme/linkNodes.json
      echo "Download of manifest definition file for theme '$theme' completed"

      for langKey in en de;
      do
         langKey=$(echo "$langKey" | tr -d '"')
         for previewSize in $(cat assets/imageDerivates.json | jq '.frontendPreview | .[]')
         do
            previewSize=$(echo "$previewSize" | tr -d '"')
            curl -s $2/assets/images/theme/$theme/preview_${langKey}_s${previewSize}.png > assets/images/theme/$theme/preview_${langKey}_s${previewSize}.png
            echo "Download of preview image '$theme/preview_${langKey}_s${previewSize}.png' completed"
         done

         curl -s $2/manifest_${theme}_${langKey}.json > manifest_${theme}_${langKey}.json
         echo "Download of manifest file for $theme ($langKey version) completed"
      done
      for logoSize in $(cat assets/imageDerivates.json | jq '.logo | .[]')
      do
         logoSize=$(echo "$logoSize" | tr -d '"')
         curl -s $2/assets/images/theme/$theme/logo_s${logoSize}.png > assets/images/theme/$theme/logo_s${logoSize}.png
         echo "Download of logo image '$theme/logo_s${logoSize}.png' completed"
      done
      echo "Download of assets for theme '$theme' completed"
      echo "-------------------------------------------"
   done
else
   echo "Styles are not equal - regenerating theme assets"
   echo "Hashdiff is"
   curl -s "$2/assets/theme-hashes.json" | diff - assets/theme-hashes.json || true

   cd "$baseDir"
   echo "Starting the ssr server"
   node dist/frontend/server/main.js &
   serverPID=$!
   sleep 3s

   cd "$buildDir/assets/jobs"
   echo "Generating link images"
   node GenerateMetaNodes.js --command=generateLinkImages --baseUrl="$2"

   echo "Generating manifest"
   node GenerateMetaNodes.js --command=generateManifest --baseUrl="$2"

   echo "Generating preview screenshots"
   node --experimental-modules GenerateImages.mjs --command=all --host=http://localhost:4000 --root=true

   kill $serverPID
fi
